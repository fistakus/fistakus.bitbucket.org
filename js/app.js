angular.module("yaih", ["ngResource", "ngMaterial", "masonry", "device"])

.config(function($mdThemingProvider) {
	$mdThemingProvider.theme("default")
		.primaryPalette("blue-grey")
		.accentPalette("deep-orange");
})

// a factory that imitates 'real' resource - every request is delayed for about 0.5 - 1.5s
// some hardcoded logic is put here as other way the page would be totally static :(
.factory("DeviceResource", ["$resource", "$q", function DeviceResourceFactory($resource, $q) {
	var Device = $resource("api/devices/:id", null, {
		"update": {
			method: "PATCH"
		}
	});
	var DeviceList = $resource("api/devices.json");

	var devices_state_that_should_be_from_backend = {
		123: {
			light: true
		},
		345: {
			power: true
		},
		678: {
			rinse: false
		}
	};

	return {
		get: function get_single_device(params, ok, error) {
			var delay = Math.random() * 1000 + 500;
			return $q(function defer() {
				setTimeout(function defer() {
					if (params.id === 123) {
						params.id += "_" + devices_state_that_should_be_from_backend[123].light;
					} else if (params.id === 345) {
						var on = devices_state_that_should_be_from_backend[345].power;
						params.id += "_" + (!on ? on : 
							on 
							+ "_light=" + devices_state_that_should_be_from_backend[123].light
							+ "_dish=" + devices_state_that_should_be_from_backend[678].rinse);
					} else if (params.id === 678) {
						params.id += "_" + devices_state_that_should_be_from_backend[678].rinse;
					}
					return Device.get(params, ok, error);
				}, delay);
			});
		},
		update: function set_single_device(params, body, ok, error) {
			var delay = Math.random() * 1000 + 500;
			return $q(function defer() {
				setTimeout(function defer() {
					if (params.id === 123) {
						devices_state_that_should_be_from_backend[123].light = body[0].state;
					} else if (params.id === 345) {
						devices_state_that_should_be_from_backend[345].power = body[0].state;
					} else if (params.id === 678) {
						devices_state_that_should_be_from_backend[678].rinse = body[0].state;
					}
					return Device.update(params, body, ok, error);
				}, delay);
			});
		},
		query: function query_list_device(ok, error) {
			var delay = Math.random() * 1000 + 500;
			return $q(function defer() {
				setTimeout(function defer() {
					return DeviceList.query().$promise.then(ok, error);
				}, delay);
			});
		}
	}

}])

.factory("Logger", ["$mdToast", function LoggerFactory($mdToast) {
	var debug_mode = false;

	function showToast(content) {
		if (debug_mode) {
			$mdToast.show(
				$mdToast.simple()
				.textContent(content)
				.hideDelay(6000)
			);
		}
	};

	return {
		logRequest: function logSingleRequest(requestName, duration) {
			showToast(requestName + " in " + duration + " ms");
		},
		logTemplateMissing: function logSingleRequest(contentType) {
			showToast("Template for " + contentType + " not found, default loaded");
		}
	}
}])

.controller("devices", function DeviceListController($scope, DeviceResource, Logger) {

	$scope.devices = [];

	var request_time_milis = new Date().getTime();
	DeviceResource.query(function(devices) {
		$scope.devices = devices;
		Logger.logRequest("OK, devices", new Date().getTime() - request_time_milis);
	});

	$scope.deleteDevice = function(device) {
		var idx = $scope.devices.indexOf(device);
		if (idx >= 0) {
			$scope.devices.splice(idx, 1);
		}
	};

})

angular.module("device", ["yaih"])
	.directive("device", ["$http", "$templateCache", "$compile", "Logger", function($http, $templateCache, $compile, Logger) {

		/**
		 * based on https://coderwall.com/p/mgtrkg/variable-templates-for-an-angularjs-directive
		 *
		 * Get a template based on device manufacturer id (industry_name)
		 * with a fallback to the device-unknown if custom template not found
		 *
		 * @param manufacturerId
		 *				String with the unique name of the device type. 
		 *				If not present the device-unknown template will be used.
		 */
		function getTemplate(manufacturerId) {

			function downloadTemplate(templateUrl) {
				return $http.get(templateUrl, {
					cache: $templateCache
				});
			}

			var templateMapDefault = "card-device-unknown",
				templateUrl = "templates/" + (manufacturerId || templateMapDefault) + ".html";

			return downloadTemplate(templateUrl).then(
				null, // if there is custom template we do not need to do anything
				function() {
					Logger.logTemplateMissing(manufacturerId);
					return downloadTemplate("templates/" + templateMapDefault + ".html");
				});
		};

		return {
			link: function(scope, element) {
				// get template for this device and display component
				getTemplate(scope.singleDevice.info.industry_name).then(function ok(response) {
					element.html(response.data).parent('*[masonry]:first').scope().update();
					$compile(element.contents())(scope);
				});
			},
			bindToController: true,
			scope: {
				info: "<",
				onDelete: "&"
			},
			controllerAs: "singleDevice",
			controller: function SingleDeviceController($scope, $timeout, $interval, $filter, DeviceResource, Logger) {
				var ctrl = this,
					in_save = false,
					refresh_interval = 10000;

				ctrl.fails = 0;
				ctrl.progress = refresh_interval / 100;
				var progress_interval = $interval(function() {
					ctrl.progress -= 1;
				}, 100, 0, true);



				$scope.delete = function() {
					$interval.cancel(progress_interval);
					progress_interval = undefined;
					ctrl.onDelete({
						info: $scope.info
					});
				};

				$scope.send_state = function send_state() {
					in_save = true;
					DeviceResource.update({
						id: $scope.singleDevice.info.device_id
					}, $scope.singleDevice.info.abilities, function() {
						in_save = false;
					}, function() {
						in_save = false;
					});
				};

				$scope.get_ability = function get_first_ability(_type) {
					return $filter("filter")($scope.singleDevice.info.abilities, {
						type: _type
					})[0];
				}

				$scope.get_sensor = function get_first_sensor(_name) {
					return $filter("filter")($scope.singleDevice.info.sensors, {
						name: _name
					})[0];
				}

				function refresh_state() {
					var request_time_milis = new Date().getTime();

					if (!in_save) {
						DeviceResource.get({
								id: $scope.singleDevice.info.device_id
							}, function got_new_state(response) {
								// new state feched from the server
								if (!in_save) {
									$scope.singleDevice.info = response;
								}
								ctrl.fails = 0;

								Logger.logRequest("OK, device id:" + $scope.singleDevice.info.device_id, new Date().getTime() - request_time_milis);
								ctrl.progress = refresh_interval / 100;
							},
							function got_error_state(response) {
								// some error occured, count it as failure
								if (response.status !== 200) {
									ctrl.fails++;
								}
								Logger.logRequest("ERROR(" + ctrl.fails + ") device id:" + $scope.singleDevice.info.device_id, new Date().getTime() - request_time_milis);
								ctrl.progress = refresh_interval / 100;
							});
					}
					// if 2 consequent refresh attempts failed remove device
					// but if everything was fine just call next refresh
					ctrl.fails >= 2 ? $scope.delete() : $timeout(refresh_state, refresh_interval);
				};

				//first refresh after 5-20s
				$timeout(refresh_state, 5000 + Math.random() * 10000);
			}
		}
	}]);