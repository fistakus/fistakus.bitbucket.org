# Demo

Live preview can be found [here](http://fistakus.bitbucket.org).

# Documentation

## Terms

###### Device Instance
representation of the single intelligent device at home
###### Card
a visual representation of a _Device Instance_ on the dashboard
###### Device Type
type of a device like: TV from company X or light bulbs from company Y which share the same interface for the end user and therefore may use the same _Card_'s layout
###### Ability
what you can ask the _Device Instance_ to do for you
###### Sensor
state feedback that you can get from the _Device Instance_

## How to add support for a new device

Please remember that this solution is based only on static data files. Any server call should be, for now, replaced with a call for a file from `/api/` folder.

Adding a new _Device Instance_ is as simple as adding new element to `/api/device.json`. This is sufficient for the app to show a default _Card_ for this device for few seconds. Each _Card_ is refreshing its state every few seconds and after 3rd error the card will dissapear (firstly showing a red progress indicator). If you wish to have the _Card_ to be displayed longer you also need to add a new file in `/api/devices/` named by the device_id (i.e. `/api/devices/123`) - the contnent should be in line with the element you added to `/api/device.json`.

Each Device description may contain:
```javascript
{
	"device_id": 123, // REQUIRED, unique identifier of the Device Instance
	"industry_name": "lg.lightbulb.12345", // identifier of the Device Type
	"description": "something", // description displayed on the Card
	"display_name": "Light bulb E27", // name of the Device Instance displayed on the Card 
	"abilities": [{
		"type": "switch", // enum: slider_cont, switch, preview - other will be displayed as unknown
		"name": "power", // name of ability to be displayed on the Card
		"false_option_name": "off", // SWITCH TYPE ONLY: name on the left side of the switch
		"true_option_name": "on", // SWITCH TYPE ONLY: name on the right side of the switch
		"state": true, // SWITCH TYPE ONLY: current state of the ability
		"min": 1, // SLIDER_CONT TYPE ONLY: name on the right side of the slider
		"max": 5 // SLIDER_CONT TYPE ONLY: name on the right side of the slider
	}],
	"sensors": [{
		"type": "preview", // enum: motion, preview - for default template
		"name": "camera", // unique name by which it is possible to filter sensors
		"state": "url/to/the/file" // current state of the sensor
	}]
}
```
This will show a default _Card_.

### Adding a custom template
If you wish to have a custom template for a _Device Type_ you may do that by adding a HTML file in `/templates/` folder with the name same as the industry_name that you choose in previous step (i.e. `/templates/lg.lightbulb.12345.html`). And that is it! Now you can have fun with designing the layout for the inside of the _Card_. The content of the _Device_ description is accessible via `singleDevice.info`.

You can find useful also:
* `singleDevice.fails` - number of consequent failed calls to `/api/devices/{id}`,
* `singleDevice.progress` - percentage of time left to next call to `/api/devices/{id}`,
* `send_state()` - function to send PATCH request with current state of all abilities.

If in doubt refer to `/templates/card-device-unknown.html` or any other custom template provided.

# Development tools

## SonarQube
Code quality analysis with plugin for JS. [Get it here](http://www.sonarqube.org/). 

When you have SonarQube and SonarScanner installed just go to project parent directory and run sonar-scanner for your file system.

## Git ignore . io
Online tool for creating .gitignore files depending on tools used in the project. Used once to generate the file. [Use it here](https://www.gitignore.io/).

# Technology stack

Please refer to this section if you wish to find more about libraries that are used in the app. For development tools goto __Development tools__ section.

NOTE: Most of the JS and CSS listed below could be loaded from CDN. No changes were made to vendor files.


## jQuery
JS in version 3.1.0 licensed under MIT License - [docs](http://api.jquery.com/).

## Angular
JS in version 1.5.8 licensed under MIT License -  [docs](http://angularjs.org/).

* angular core
* angular animate
* angular aria
* angular resource

## Angular Material
JS, CSS in version 1.1.0 licensed under MIT License -  [docs](https://material.angularjs.org/1.1.0/).

## Masonry
JS in version 4.1.1 licensed under MIT License - [docs](http://masonry.desandro.com/).

### Masonry angular directive
JS - author Klederson Bueno, no information about the license - [docs](https://github.com/klederson/angular-masonry-directive).

### Images Loaded
JS in version 4.1.0 licensed under MIT License - [docs](http://imagesloaded.desandro.com/).

## Material Icons
CSS and Fonts in version 2.2 licensed under CC-BY 4.0 - [docs](https://design.google.com/icons/).

## Placehold . it
Images on demand in custom size. License unknown but with description "Provided for free by a guy named Brent" - [docs](https://placehold.it/).
